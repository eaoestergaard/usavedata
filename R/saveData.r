library(readxl)
library(xts)
startDate = as.POSIXlt(as.Date("1926-01-01"))
endDate=as.POSIXlt(as.Date("2020-09-01"))
IBBSData <- read_excel("R/IBBS-stocks-bonds-bills-and-inflation-data_2020_V09012021_01.xlsm",
                       skip = 11,
                       trim_ws = TRUE,
                       #range = "A11:P9999",
                       col_names = FALSE,
                       col_types = c("date", "numeric", "numeric",
                                     "numeric", "numeric", "numeric",
                                     "numeric", "numeric", "numeric",
                                     "numeric", "numeric", "numeric",
                                     "numeric", "numeric", "numeric",
                                     "numeric"))


cn<-gsub(" |:|","",
         c("Date",
           read_excel("R/IBBS-stocks-bonds-bills-and-inflation-data_2020_V09012021_01.xlsm",
                      sheet = "--Monthly SBBI Data--",
                      range = "B10:P10",
                      col_names = FALSE)))



colnames(IBBSData) = cn

totalReturn =IBBSData[IBBSData$Date >= startDate & IBBSData$Date <= endDate,c(1,2,5,6,7,15,16)]

names(totalReturn)<-c("Date","LargeStocks", "SmallStocks","CorporateBonds","GovernmentBonds","TBill", "Inflation")

save(totalReturn, file = "R/totalReturn.rda")

totalReturnInflationAdjusted = totalReturn
totalReturnInflationAdjusted[2:7] = (1+totalReturn[,2:7])/(1+totalReturn$Inflation)-1

save(totalReturnInflationAdjusted, file = "R/totalReturnInflationAdjusted.rda")
